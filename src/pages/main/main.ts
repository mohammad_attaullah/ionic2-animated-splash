import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { AnimationBuilder, AnimationService, AnimatesDirective } from 'css-animator'

@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage {
  splash = true;
  @ViewChild('myElement') myElem: ElementRef;
  @ViewChild('rubberBand') rBand: ElementRef;
  @ViewChild('zoomIn') zoomIn: ElementRef;
  @ViewChild('pulse') pulseIn: ElementRef;
  @ViewChild('bounceOut') bOut: ElementRef;
  @ViewChild('fadeIn') fUp: ElementRef;
  private animator: AnimationBuilder;

  constructor(public navCtrl: NavController, animationService: AnimationService) {
    this.animator = animationService.builder();
    var intervalId = setInterval(() => {
      this.animateElem();
    }, 1500);
    setTimeout(() => {
      clearInterval(intervalId);
      this.splash = false;
    }, 8000)
  }

  animateElem(): void {
    this.animator.setType('bounce').show(this.myElem.nativeElement);
  }

  moveRubberBand(): void {
    this.animator.setType('rubberBand').show(this.rBand.nativeElement);
  }
  zoomItIn(): void {
    this.animator.setType('zoomIn').show(this.zoomIn.nativeElement);

  }
  pulseIt(): void {
    this.animator.setType('pulse').show(this.pulseIn.nativeElement);

  }
  bounceItOut(): void {
    this.animator.setType('bounceOut').show(this.bOut.nativeElement);

  }
  fadeInFunc(): void {
    this.animator.setType('fadeInUp').show(this.fUp.nativeElement);

  }
}
